## GitOps Repository

### Overview

**What is GitOps?**

Continuous Delivery where Git acts as the source of truth for declarative infrastructure and workloads.

For Kubernetes this means using `git push` instead of `kubectl create/apply`.

In the GitOps pipeline model, any change to production must be committed in source control (preferably via a pull request) prior to the change being applied. This way rollback and audit logs are provided by Git.

If the entire production state is under version control and described in a single Git repository, when disaster strikes, the whole infrastructure can be quickly restored from that repository.

### Manifests
- - - -

Kubernetes manifests are used to create, modify and delete Kubernetes resources such as pods, deployments, services or ingresses.

It is very common to define manifests in form of .yaml files and send them to the Kubernetes API Server via commands such as `kubectl apply -f my-file.yaml` or `kubectl delete -f my-file.yaml`.

Manifests are stored under directory `manifests`.

### Charts
- - - -

Helm uses a packaging format called charts. A chart is a collection of files that describe a related set of Kubernetes resources.

A single chart might be used to deploy something simple, like a memcached pod, or something complex, like a full web app stack with HTTP servers, databases, caches, and so on.

Charts are stored under directory `charts`.

The DevOps team manages a collection of charts used to deploy various workloads (i.e: node, java) located [here](https://transcore.jira.com/wiki/spaces/AD/pages/642318380/Helm).

An example deployment of a Helm chart can be found in `charts/example`.
