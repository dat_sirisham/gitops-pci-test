### Overview

Deployment of a NodeJS containerized application via Helm Charts.

This deployment used the NodeJS chart provided by the DevOps team. Full details, on the available values to this chart can be found [here](https://bitbucket.org/dat/helm/src/master/node/).

#### Resources

After this chart has been applied to the cluster the following Kubernetes Resources will exist

   * example-microservices helmRelease
   * example-microservices helm chart
   * example-microservices deployment
   * two running PODs of image dat-docker.jfrog.io/example-microservice:test-e4f2a99

The running containers will be allocated 256Mi of memory and 1/10th of a CPU. In addition the container will be limited to 256Mi of memory.


#### Access

This example will expose the service created by this application at URL /example/.
