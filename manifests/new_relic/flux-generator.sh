#!/usr/bin/env bash

set -o pipefail
set -o errexit

# hard coded (because these never change)
declare -A ENV_ACCOUNT_ID_MAP
ENV_ACCOUNT_ID_MAP[non-prod]=101678534766
ENV_ACCOUNT_ID_MAP[prod]=700178098589
ENV_ACCOUNT_ID_MAP[dev]=755621335444

# determine our instance-id from the metadata service
INSTANCE_ID=`wget -qO- http://169.254.169.254/latest/meta-data/instance-id`
REGION=`wget -qO- http://169.254.169.254/latest/meta-data/placement/availability-zone | sed 's/\(.*\)[a-z]/\1/'`
export AWS_DEFAULT_REGION=$REGION

# query various tags assigned to our instance (ENVIRONMENT, DEPLOYMENT and DOMAIN)
DEPLOYMENT=`aws ec2 describe-tags --filters "Name=resource-id,Values=${INSTANCE_ID}" \
                     --query 'Tags[?Key==\`deployment\`].Value' --output text`
DOMAIN=`aws ec2 describe-tags --filters "Name=resource-id,Values=${INSTANCE_ID}" \
                --query 'Tags[?Key==\`domain\`].Value' --output text`
ENVIRONMENT=`aws ec2 describe-tags --filters "Name=resource-id,Values=${INSTANCE_ID}" \
                 --query 'Tags[?Key==\`environment\`].Value' --output text`

# grab necessary configuration for New Relic (values are base64 encoded)
NEW_RELIC_INTEGRATION_ENABLED=ZmFsc2UK
if [[ $DEPLOYMENT == "test" || $DEPLOYMENT == "prod" ]]; then
  # set to true
  NEW_RELIC_INTEGRATION_ENABLED=dHJ1ZQo=
fi

# configuration is stored in the environment (non-prod or prod) SSM Parameter store
#
# Therefore we have to assume a role within that account in order to query
KST=(`aws sts assume-role --role-arn "arn:aws:iam::${ENV_ACCOUNT_ID_MAP[$ENVIRONMENT]}:role/DATParameterStore-ReadOnly" \
                          --role-session-name "flux-${DOMAIN}-${ENVIRONMENT}-read-parameter-store" \
                          --duration-seconds 900 \
                          --query '[Credentials.AccessKeyId,Credentials.SecretAccessKey,Credentials.SessionToken]' \
                          --output text || exit 0`)

# if we fail at this point, exit script (as we must not have the necessary permissions yet; flux will try again)
export AWS_ACCESS_KEY_ID=${KST[0]}
export AWS_SECRET_ACCESS_KEY=${KST[1]}
export AWS_SESSION_TOKEN=${KST[2]}

NEW_RELIC_LICENSE_KEY=`aws ssm get-parameter --name "/${DOMAIN}/newrelic/license_key" \
                               --query 'Parameter.Value' --output text 2> /dev/null || exit 0`

NEW_RELIC_API_KEY=`aws ssm get-parameter --name "/${DOMAIN}/newrelic/api_key" \
                               --query 'Parameter.Value' --output text 2> /dev/null || exit 0`

# create our transformation file that will be used by flux generator
cat << EOF > /tmp/flux.sed
s/%DEPLOYMENT%/${DEPLOYMENT}/g
s/%NEW_RELIC_LICENSE_KEY%/${NEW_RELIC_LICENSE_KEY}/g
s/%NEW_RELIC_API_KEY%/${NEW_RELIC_API_KEY}/g
s/%NEW_RELIC_INTEGRATION_ENABLED%/${NEW_RELIC_INTEGRATION_ENABLED}/g
EOF

sed -f /tmp/flux.sed new_relic/secret.yaml
